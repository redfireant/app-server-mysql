# Summary

The MySQL App Server role sets up the MySQL server

This role is used to setup the following:

* Installs the application server and configures the server to be bonded to a green (internal) IP
* Installs a MySQL client for apps as a task
* Sets up a application specific database as a task

## Legacy Server Variables

| Variable name            | Data Type | Description                                                                  | File   |
|--------------------------|-----------|------------------------------------------------------------------------------|--------|
| mysql_container_name     | string    | MySQL container name                                                         | group  |
| mysql_version            | string    | MySQL version. Defaults to latest                                            | group  |
| mysql_data_directory     | string    | Default data directory                                                       | group  |
| mysql_config_directory   | string    | Default config directory                                                     | group  |
| mysql_backup_directory   | string    | Default backup directory                                                     | group  |
| mysql_root_password      | string    | Super admin password                                                         | group  |
| mysql_external_port      | integer   | Externally accessible port. Defaults to 3306                                 | group  |
| mysql_log_directory      | string    | Any specific logs                                                            | group  |

## Settings

| Variable name            | Description                            | Values                                          | File   |
|--------------------------|----------------------------------------|-------------------------------------------------|--------|
| mysql_install_type       | Defines the install type               | server; docker                                  | group  |